import express from 'express';

const app = express();
const puerto = 3000;

// ruta 

app.get("/sumar", (peticion, respuesta) => {
    let resultado = Number(peticion.query.a) + Number(peticion.query.b);
    respuesta.send(resultado.toString());
})

app.get("/restar", (peticion, respuesta) => {
    let resultado = Number(peticion.query.a) - Number(peticion.query.b);
    respuesta.send(resultado.toString());
})

app.get("/multi", (peticion, respuesta) => {
    let resultado = Number(peticion.query.a) * Number(peticion.query.b);
    respuesta.send(resultado.toString());
})

app.get("/dividir", (peticion, respuesta) => {
        if (Number(peticion.query.b) == 0) {
            respuesta.send("El valor b no puede ser cero");
        } else {
            let resultado = Number(peticion.query.a) / Number(peticion.query.b);
            respuesta.send(resultado.toString());
        }

    })
    // iniciar servidor 

app.listen(puerto, () => { console.log("Todo bien con el servi 3000") });